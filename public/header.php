<!DOCTYPE html>
<html lang="en">
<head>
    
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/public/style.css">
    <title> &#127796; Decouvre la Reunion &#127796;</title>
</head>
<body>
    <header>

        <div id=tete>
            <h1>
                <br/>
                <div id=bro>www.Mi-aime-a-ou.com</div>
                <br/>
            </h1>

        </div>

        <br/>

        <h3>
            &#127796; <u><str>La Réunion</str>, Guide touristique, toutes les infos pour préparer votre voyage avec <str>Mi-aime-a-ou.com</str></u>. &#127796;
        </h3>

        <br/>

        <div id=intro>

            <figure>
                <img src="/public/images/la_reunion.jpg" alt="Photos de l'ile" width="80%" /></br>
            </figure>

            <br/>

            <p>
                La Réunion est une destination de rêve, avec Mi-aime-a-ou.com découvrez cette île volcanique, organisez votre séjour ou votre voyage, </br>
                trouvez toutes les infos pour préparer vos vacances. Réservez votre <a href="http://www.mi-aime-a-ou.com/hotel_residence_hoteliere.php" title="hôtel La Réunion" target="_top">hôtel</a>, votre <a href="http://www.mi-aime-a-ou.com/location_gite_chambre_hotes.php" title="gîte chambre d'hôte La Réunion" target="_top">gîte ou chambre d'hôte</a>, <a href="http://www.mi-aime-a-ou.com/location_voitures_la_reunion.php" title="Locations de voitures La Réunion" target="_top">, préparez vos visites. </br>
                Baptisée l'île intense, La Réunion offre ses lagons, ses sentiers de randonnées, ses paysages grandioses aux visiteurs, suivez le guide ! </br>
            </p>
            
            <br/>

            <p>
                <strong><a href="http://www.mi-aime-a-ou.com" target="_top" title="La Réunion Mi-aime-a-ou.com">La Réunion Mi-aime-a-ou.com</a> :: <a href="http://www.mi-aime-a-ou.com/rechercher_sur_mi-aime-a-ou.php" target="_top" title="Rechercher une page sur Mi-aime-a-ou.com">Rechercher une page</a> </strong>
            </p>
        </div>
            

    </header>
