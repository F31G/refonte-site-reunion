
<?php include('header.php');?>

    <div id=container1>

        <div id=item1>
            <img src="/public/images/reunion_01.jpg" alt="Photos de l'ile" width=600 /></br>
            <u>Préparer votre voyage à La Réunion</u>
        </div>

        <div id=item2>
            <img src="/public/images/reunion_02.jpg" alt="Photos de l'ile" width=600 /></br>
            <u>Annuaire touristique La Réunion</u>
        </div>

        <div id=item3>
            <img src="/public/images/reunion_05.jpg" alt="Photos de l'ile" width=600 /></br>
            <u>Présentation de La Réunion</u>
        </div>

        <div id=item4>
                <img src="/public/images/reunion_04.jpg" alt="Photos de l'ile" width=600 /></br>
                <u>Les 24 communes de La Réunion</u>
        </div>

        <div id=item5>
            <img src="/public/images/reunion_08.jpg" alt="Photos de l'ile" width=600 /></br>
            <u>Flore de La Réunion</u>
        </div>

        <div id=item6>
            <img src="/public/images/reunion_09.jpg" alt="Photos de l'ile" width=600 /></br>
            <u>Faune La Réunion</u>
        </div>

        <div id=item7>
            <img src="/public/images/reunion_03.jpg" alt="Photos de l'ile" width=600 /></br>
            <u>Photos La Réunion</u>
        </div>

        <div id=item8>
            <img src="/public/images/reunion_07.jpg" alt="Photos de l'ile" width=600 /></br>
            <u>Météo climat La Réunion</u>
        </div>

        <div id=item9>
            <img src="/public/images/reunion_11.jpg" alt="Photos de l'ile" width=600 /></br>
            <u>Recettes de cuisine créole</u>
        </div>

        <div id=item10>
            <img src="/public/images/reunion_10.jpg" alt="Photos de l'ile" width=600 /></br>
            <u>Randonnée balade à La Réunion</u>
        </div>

    </div>

    </br>
    </br>

<?php include('footer.php');?>